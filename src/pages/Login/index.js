import React, {Component} from 'react';
import { Layout, Form, Input, Button } from 'antd';
import s from './Login.module.scss';
import { withFirebase } from "../../Context/firebaseContext";
import {Link} from "react-router-dom";

const { Content } = Layout;

class LoginPage extends Component {
    onFinish = ({email, password}) => {
        const {signWithEmail} = this.props.firebase;
        const {history} = this.props;

        signWithEmail(email, password)
            .then(res => {
                console.log('###: res', res);
                history.push('/');
            })
    }

    onFinishFailed = (errorMsg) => {
        console.log('###: errorMsg', errorMsg);
    }

    renderForm = () => {
        const layout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 18 },
        };
        const tailLayout = {
            wrapperCol: { offset: 6, span: 17 },
        };

        return(
            <Form
                {...layout}
                name="basic"
                initialValues={{ remember: true }}
                onFinish={this.onFinish}
                onFinishFailed={this.onFinishFailed}
            >
                <Form.Item
                    label="Email"
                    name="email"
                    rules={[{ required: true, message: 'Please input your email!' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    rules={[{ required: true, message: 'Please input your password!' }]}
                >
                    <Input.Password />
                </Form.Item>
                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                    <Link to="/Register">Register</Link>
                </Form.Item>
            </Form>
        );
    }

    render() {
        console.log('###: ', this.props);

        return (
            <Layout>
                <Content>
                    <div className={s.root}>
                        <div className={s.form_wrap}>
                            {this.renderForm()}
                        </div>

                    </div>
                </Content>
            </Layout>
        );
    }
}

export default withFirebase(LoginPage);