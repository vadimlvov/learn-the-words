import React, {Component} from 'react';
import { Layout, Form, Input, Button } from 'antd';
import s from './Register.module.scss';
import FirebaseContext from "../../Context/firebaseContext";
import {Link} from "react-router-dom";

const { Content } = Layout;

class RegisterPage extends Component {
    onFinish = ({email, password}) => {
        const {signUpWithEmail} = this.context;
        const {history} = this.props;

        signUpWithEmail(email, password)
            .then(res => {
                console.log('###: res', res);
                history.push('/');
            })
    }

    onFinishFailed = (errorMsg) => {
        console.log('###: errorMsg', errorMsg);
    }

    renderForm = () => {
        const layout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 18 },
        };
        const tailLayout = {
            wrapperCol: { offset: 6, span: 17 },
        };

        return(
            <Form
                {...layout}
                name="basic"
                initialValues={{ remember: true }}
                onFinish={this.onFinish}
                onFinishFailed={this.onFinishFailed}
            >
                <Form.Item
                    label="Email"
                    name="email"
                    rules={[{ required: true, message: 'Please input your email!' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    rules={[{ required: true, message: 'Please input your password!' }]}
                >
                    <Input.Password />
                </Form.Item>
                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit">
                        Register
                    </Button>
                    <Link to="/Login">Login</Link>
                </Form.Item>
            </Form>
        );
    }

    render() {


        return (
            <Layout>
                <Content>
                    <div className={s.root}>
                        <div className={s.form_wrap}>
                            {this.renderForm()}
                        </div>

                    </div>
                </Content>
            </Layout>
        );
    }
}

RegisterPage.contextType = FirebaseContext;

export default RegisterPage;