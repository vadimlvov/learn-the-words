import  React, {Component} from 'react';
import database from '../../Services/firebase'
import HeaderBlock from '../../companents/HeaderBlock';
import Header from '../../companents/Header';
import Paragraph from '../../companents/Paragraph/Index';
import {ReactComponent as ReactLogo} from '../../logo.svg';
import CardList, {hoc} from '../../companents/CardList';
import Card from "../../companents/Card";

import s from './Home.module.scss'
import TestContext from "../../Context/testContext";
import FirebaseContext from "../../Context/firebaseContext";

class HomePage extends Component{
    state = {
        wordArr: [],
    }

    urlRequest = `/cards/${this.props.user.uid}`;
    componentDidMount() {
        const {getUserCardsRef} = this.context;
        getUserCardsRef().on('value', res => {
            console.log('###: res', res.val());
            this.setState({
                wordArr: res.val() || []
            }, this.setNewWord);
        });
    }

    handleSubmitedForm = (newItem) => {
        const {wordArr} = this.state;
        const {ru, en} = newItem;
        const newArr = [
            ...wordArr,
            {
                en,
                ru,
                _id: Math.floor(Math.random() * Math.floor(1000))
            }
        ]
        database.ref(this.urlRequest).set(newArr);
    }

    setNewWord = () => {
        database.ref('/cards/' + this.state.wordArr.length).set({
            eng: 'mouse',
            rus: 'мышь',
            id: Math.floor(Math.random() * Math.floor(100))
        });
    }

    handleDeletedItem = (id) => {
        const {wordArr} = this.state;

        const newWordArr = wordArr.filter(item => item.id !== id);

        database.ref(this.urlRequest).set(newWordArr);
    }

    handleAddItem = (newItem) => {
        this.setState(({wordArr})=> {
            console.log('###: Added ', newItem);
            const {ru, en} = newItem;
            const newWordArr = [
                ...wordArr,
                {
                    en,
                    ru,
                    _id: Math.floor(Math.random() * Math.floor(1000))
                }
            ];
            return {
                wordArr: newWordArr
            }
        });
    }

    render(){
        const {wordArr} = this.state;
        console.log('###: ', this.props.user.uid);
        return(
            <>
                <HeaderBlock >
                    <Header>Время учить слова онлайн.</Header>
                    <Paragraph>Используйте карточки для запоминания и пополняйте словарный запас.</Paragraph>
                </HeaderBlock>
                <TestContext.Consumer>
                    {
                        (value => {
                            console.log('###: value', value);
                            return (
                                <CardList onAddItem={this.handleSubmitedForm} onDeletedItem={this.handleDeletedItem} item={wordArr} />
                            );
                        })
                    }
                </TestContext.Consumer>
                <FirebaseContext.Consumer>
                    {
                        ( {getUserCardRef }) => {
                            const CardEngList = hoc(Card, getUserCardRef);
                            return <CardEngList/>
                                }
                    }
                </FirebaseContext.Consumer>

                <HeaderBlock hideBackground>
                    <Header>Ещё один заголовок.</Header>
                    <Paragraph>Ну здорово же.</Paragraph>
                    <ReactLogo />
                </HeaderBlock>
            </>
        );
    }
}

HomePage.context = FirebaseContext;

export default HomePage;