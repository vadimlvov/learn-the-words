import React, { Component } from 'react';
import {Spin} from 'antd';
import Card from '../Card';
import s from './CardList.module.scss';
import getTranslateWord from "../../Services/yandex-dictionary";

export const hoc = (Component, getData) => {
    return class extends React.PureComponent {

        state = {
            value: '',
            label: '',
            isBusy: false,
            items: [],
        }

        componentDidMount() {
            getData().once('value').then(res => {
                this.setState({
                    items: res.val(),
                });
            });
        }

        handleInputChange = (e) => {
            //console.log('### value: ',e.target.value);
            this.setState({
                value: e.target.value
            });
        }

        getWord = async () => {

            const {value} = this.state;

            this.props.onAddItem(this.state.value);

            const getWordValue = await getTranslateWord(value);

            const translateWord = getWordValue[0].tr[0].text;

            this.setState( {
                label: `${value} - ${translateWord}`,
                value: '',
                isBusy: false,
            })
        }

        handleSubmitForm = async () => {
            this.setState({
                isBusy: true,
            }, this.getWord)
        }

        handleFinish = (values) => {
            console.log('Finish:', values);
            this.props.onAddItem(values);
        };

        render(){
            console.log('###: context', this.context);
            const {item = [], onDeletedItem,} = this.props;
            const {items} = this.state;

            if (items.length === 0) {
                return <Spin/>;
            }

            return (
                <>
                    <div className={s.root}>
                        {items.map(({ _id, ...props}) =>(
                            <Component
                                key = {_id}
                                {...props}
                            />
                            ))
                        }
                    </div>
                </>
            );
        }
    };
}

export default hoc(Card);