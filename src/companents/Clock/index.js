import React from 'react';
import s from './Clock.module.scss';
import {Typography} from "antd";

const {Title} = Typography;

class Clock extends React.PureComponent {
    state = {
        date: this.props.currentDate
    };

    constructor(props) {
        super(props);
        console.log('###: constructor');
    }

    componentDidMount() {
        this.interval = setInterval(this.tick, 1000);
        console.log('###: componentDidMount');
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        console.log('###: shouldComponentUpdate');
        if (this.state.date < nextState.date){
            return false;
        }

        return true;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log('###: componentDidUpdate');
        //if (this.state != prevState) {
        //    this.setState({})
        //}
    }

    componentWillMount() {
        clearInterval(this.interval);
    }

    tick = () => {
        this.setState({
            date: new Date()
        });
    }

    render() {
        const {date} = this.state;

        return (
            <>
                <Title level={2}>
                    Сейчас {date.toLocaleString()}
                </Title>
            </>
        );
    }
}

export default Clock;