import React, {Component} from 'react';
import HomePage from "./pages/Home";
import LoginPage from "./pages/Login";
import { Layout, Spin, Menu } from 'antd';

import {BrowserRouter, Route, Link} from 'react-router-dom';

import s from './App.module.scss'
import TestContext from "./Context/testContext";
import FirebaseContext from "./Context/firebaseContext";

const { Header, Content } = Layout;

class App extends Component{
  state = {
    user: null,
    count: 1,
  }

  componentDidMount() {
    console.log('###: context', this.context);
    const {auth, setUserUid} = this.context;
    auth().onAuthStateChanged((user) =>{
      console.log('###: onAuthStateChanged');
      if (user){
        setUserUid(user.uid);
        this.setState({
          user,
        });
      }else{
        setUserUid(null);
        this.setState({
          user: false,
        });
      }
    });

    setInterval(() => {
      this.setState(({count}) => {
        return {
          count: ++count,
        }
      })
    }, 3000)
  }
  
  render(){
    const {user, count} = this.state;

    if (user == null){
      return (
          <div className={s.loader_wrap}>
            <Spin size="large" />
          </div>
      );
    }
    return(
        <BrowserRouter>
          <Route path="/login" component={LoginPage} />
          <Route render={(props) => {
            console.log('###: props', props);
            const {history: {push}} = props;
            return (
                <Layout>
                  <Header>
                    <Menu theme="dark" mode="horizontal">
                      <Menu.Item key="1">
                        <Link to="/">Home</Link>
                      </Menu.Item>
                      <Menu.Item key="2">
                        <Link to="/">About</Link>
                      </Menu.Item>
                      <Menu.Item key="3" onClick={() => push('/contact')}>Contact</Menu.Item>
                    </Menu>
                  </Header>
                  <Content>
                    <Route path="/" exact component={HomePage} />
                    <Route path="/home" component={HomePage} />
                    <Route path="/about" render={() => <h1>Немного о себе...</h1>} />
                  </Content>
                </Layout>
            );
          }} />

        </BrowserRouter>
    );
  }
}

App.contextType = FirebaseContext;

export default App;
