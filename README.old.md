const key = 'trnsl.1.1.20200522T020635Z.0cfb7a2f4a6da930.c623535e18820260b83439c47edbe79416fed9d9';
const lang = 'en-ru';
const text = 'time';

const getTranslateWord = async () => {
    const res = await fetch('https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=${key}&lang=${lang}&text=${text}');
    const body = await res.json();
    
    return body;
}

getTranslateWord.then(res => console.log('###: res', res));

fetch('https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=${key}&lang=${lang}&text=${text}')
.then(res => res.json())
.then(body => console.log('###: body', body))

const wordList = [
  {
    eng: 'between',
    rus: 'между',
    id: 1
  },
  {
    eng: 'high',
    rus: 'высокий',
    id: 2
  },
  {
    eng: 'really',
    rus: 'действительно',
    id: 3
  },
  {
    eng: 'something',
    rus: 'что-нибудь',
    id: 4
  },
  {
    eng: 'most',
    rus: 'большинство',
    id: 5
  },
  {
    eng: 'another',
    rus: 'другой',
    id: 6
  },
  {
    eng: 'much',
    rus: 'много',
    id: 7
  },
  {
    eng: 'family',
    rus: 'семья',
    id: 8
  },
  {
    eng: 'own',
    rus: 'личный',
    id: 9
  },
  {
    eng: 'out',
    rus: 'из/вне',
    id: 10
  },
  {
    eng: 'leave',
    rus: 'покидать',
    id: 11
  },
];

CardList/index.js old

import React, { Component } from 'react';
import Card from '../Card';
import { Input } from 'antd';
import s from './CardList.module.scss';
import getTranslateWord from "../../Services/yandex-dictionary";

const { Search } = Input;

class CardList extends Component {
    state = {
        value: '',
        label: '',
        isBusy: false,
    }

    handleInputChange = (e) => {
        //console.log('### value: ',e.target.value);
        this.setState({
            value: e.target.value
        });
    }

    getWord = async () => {

        const {value} = this.state;

        this.props.onAddItem(this.state.value);

        const getWordValue = await getTranslateWord(value);

        const translateWord = getWordValue[0].tr[0].text;

        this.setState( {
                label: `${value} - ${translateWord}`,
                value: '',
                isBusy: false,
        })
    }

    handleSubmitForm = async () => {
        this.setState({
            isBusy: true,
        }, this.getWord)
    }

    render(){
        const {item = [], onDeletedItem,} = this.props;
        const {value, label, isBusy} = this.state;
        return (
            <>
                <div>
                    {label}
                </div>
                <div className={s.form}>
                    <Search
                        placeholder="input search text"
                        enterButton="Search"
                        size="large"
                        loading={isBusy}
                        value={value}
                        onChange={this.handleInputChange}
                        onSearch={this.handleSubmitForm}
                    />
                </div>
                <div className={s.root}>
                {
                    item.map(({eng, rus, id}) => (
                        <Card
                            onDeleted={()=>{
                                console.log('### 2 level');
                                onDeletedItem(id);
                            }} key={id} eng={eng} rus={rus} />
                    ))
                }
                </div>
            </>
        );
    }
}

export default CardList;


<CardList onAddItem={this.handleSubmitedForm} onDeletedItem={this.handleDeletedItem} item={wordArr} />

<>
        {user ? (
            <TestContext.Provider value={{uid: user.uid, count}}>
            <HomePage user = {user}></HomePage>
            </TestContext.Provider>
        ) : <LoginPage />}
      </>